<?php

namespace _7_abstract_interface_trait;

/**
 * ________________________________________________________________________
 *                          CLASSES ABSTRAITES
 * ________________________________________________________________________
 *
 * Les classes abstraites sont des classes qui ne peuvent pas être instanciées.
 *
 * Les classes abstraites peuvent inclure des méthodes abstraites qui permettent de
 * déclarer une signature de fonction sans mettre en place l'implémentation.
 *
 * Les classes héritant de la classe abstraite doivent définir toutes les méthodes
 * abstraites et les implémenter.
 *
 * Les classes abstraites servent à partager des fonctionnalités communes entre
 * plusieurs classes enfants et / ou à définir un ensemble de règles d'implémentation.
 */
//abstract class User
//{
//    // partager $nom
//    public function __construct(public string $nom) {}
//
//    // forcer l'implémentation de la méthode greeting()
//    abstract public function greeting(): string;
//}
//
//class Admin extends User
//{
//    public function greeting(): string
//    {
//        return "Je suis un admin et je m'appelle $this->nom !";
//    }
//}
//
//class Moderator extends User
//{
//    public function greeting(): string
//    {
//        return "Je suis un modérateur et je m'appelle $this->nom !";
//    }
//}
//
//class Registered extends User
//{
//    public function greeting(): string
//    {
//        return "Je suis un utilisateur et je m'appelle $this->nom !";
//    }
//}
//
//$admin = new Admin("Jean");
//echo $admin->greeting() . PHP_EOL;
//
//$modo = new Moderator("John");
//echo $modo->greeting() . PHP_EOL;
//
//$user = new Registered("Kevin");
//echo $user->greeting() . PHP_EOL;

// une classe abstraite ne peut pas être instanciée
//$user = new User();


/**
 * ________________________________________________________________________
 *                              INTERFACES
 * ________________________________________________________________________
 *
 * Les interfaces permettent de définir quelles méthodes une classe doit implémenter,
 * sans avoir à définir leur implémentation.
 *
 * Toutes les méthodes de l'interface doivent être implémentées dans une classe
 * qui implémente l'interface.
 *
 * Une interface est un contrat pour une classe. Elle permet d'obliger à ce qu'une
 * ou plusieurs classes respectent un modèle.
 *
 * Contrairement à une classe abstraite, une interface n'implémente aucune logique.
 */

//interface Database
//{
//    function createOrder();
//
//    function getOrder();
//
//    function updateOrder();
//
//    function removeOrder();
//
//    function listOrders();
//}
//
//interface X
//{
//    function createOrder();
//}
//
//class SQLDatabase implements Database, X
//{
//    function createOrder()
//    {
//        // Implémentation
//    }
//
//    function getOrder()
//    {
//        // Implémentation
//    }
//
//    function updateOrder()
//    {
//        // Implémentation
//    }
//
//    function removeOrder()
//    {
//        // Implémentation
//    }
//
//    function listOrders()
//    {
//        // Implémentation
//    }
//}

/**
 * ________________________________________________________________________
 *                              TRAITS
 * ________________________________________________________________________
 *
 * Les traits permettent de réutiliser du code dans plusieurs classes.
 *
 * Un trait est un ensemble de fonctionnalités qui peut être facilement partagé
 * entre plusieurs classes.
 *
 * Pour utiliser un trait dans une classe, il suffit de faire use nomDuTrait.
 *
 * Il est possible d'utiliser plusieurs traits dans la même classe.
 *
 * Vous pouvez voir un trait comme un moyen d'éviter la duplication.
 * Dès que vous avez du code dupliqué entre différentes classes, et que ces classes
 * n'ont pas de relation d'héritage, c'est là qu'il faut utiliser un trait
 */

trait Trait1
{
    function actionX() {
        echo 'Trait 1 : Action X' . PHP_EOL;
    }

    function actionY() {
        echo 'Trait 1 : Action Y' . PHP_EOL;
    }
}

trait Trait2
{
    public $compteur = 0;

    function compter()
    {
        $this->compteur++;
        echo 'Trait 2 - compteur : ' . $this->compteur . PHP_EOL;
    }
}

class BaseTrait
{
    use Trait1, Trait2;
}

class BaseTrait2
{
    use Trait1, Trait2;
}

$baseTrait = new BaseTrait();
$baseTrait2 = new BaseTrait2();

$baseTrait->compter();
$baseTrait2->compter();