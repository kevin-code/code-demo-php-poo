
# Code de la démo php / poo

Code de la présentation php programmation orientée objet du **20/04/23** 


## Authors

- [@KevinJollis](https://www.linkedin.com/in/kevin-jollis-0a33631b9/)


## Badges

[![MIT License](https://img.shields.io/static/v1?label=SLAGSOA&message=DEMO&color=yellow)](https://choosealicense.com/licenses/mit/)

[![MIT License](https://img.shields.io/static/v1?label=PHP&message=POO&color=blue)](https://choosealicense.com/licenses/mit/)


