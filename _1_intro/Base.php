<?php

namespace _1_intro;

class Base
{
    // variable = propriété
    public string $var = 'une valeur';

    // fonction = méthode
    public function helloWorld()
    {
        echo 'Hello World !';
    }
}

/**
 * Pour instancier une classe (créer un objet), on utilise le mot clé new
 * _______________________________________________________________________
 */

$objet = new Base();
var_dump($objet);

/**
 * Pour accéder aux propriétés et aux méthodes, on utilise '->' suivi du nom de la propriété ou méthode
 * _________________________________________________________________________________________________
 */

echo $objet->var . PHP_EOL; // une valeur
echo $objet->helloWorld() . PHP_EOL; // Hello World !

/**
 * /!\ L'assignation d'un objet désigne le même objet
 * L'objet n'est pas copié, mais c'est le pointeur vers l'objet qui est copié
 * _________________________________________________________________________________________________
 */

$objet2 = new Base();
$var2 = $objet2;

echo $objet2->var . PHP_EOL; // une valeur
echo $var2->var . PHP_EOL; // une valeur

$var2->var = '42';

echo $objet2->var . PHP_EOL; // 42
echo $var2->var . PHP_EOL; // 42

/**
 * Le mot clé $this : permet de désigner l'objet courant.
 * Il s'utilise dans une méthode, il sera remplacé par l'objet qui utilise la méthode.
 * ________________________________________________________________________________________
 */

class Class2 {
    public string $nom = 'John';

    public function direBonjour() {
        echo "Bonjour $this->nom !";
    }
}

$class2 = new Class2();
echo $class2->direBonjour(); // Bonjour John
