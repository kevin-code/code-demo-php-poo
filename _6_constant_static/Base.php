<?php

namespace _4_constantes_static;

/**
 * ________________________________________________________________________
 *                     PROPRIETES ET METHODES STATIC
 * ________________________________________________________________________
 * Les propriétés et les méthodes statiques sont des méthodes disponibles sur la classe elle-même,
 * et sur les instances de la classe
 */

class MyClass
{
    public static string $year = '2023';
}

// Avantage : j'accède aux propriétés et méthodes statiques sans instancier ma classe
echo MyClass::$year . PHP_EOL;

// On utilise (::) et non l'opérateur (->) pour accéder aux propriétés et méthodes statiques
$myclass = new MyClass();
echo $myclass::$year . PHP_EOL;

/**
 * ________________________________________________________________________
 *                          CONSTANTES DE CLASSE
 * ________________________________________________________________________
 * Les constantes de classe sont des constantes déclarées sur une classe.
 * Ce sont des propriétés non modifiables. La visibilité par défaut des constantes est publique.
 */

class ParentClass
{
    const PI = 3.1415926535898;

    static function Parentfunc()
    {
        echo 'ParentClass : ' . self::PI . PHP_EOL;
        echo 'ParentClass : ' . static::PI . PHP_EOL;
    }
}

class ChildClass extends ParentClass
{
    static function Childfunc()
    {
        echo 'ChildClass : ' . parent::PI . PHP_EOL;
    }
}

echo 'Accès à la constante : ' . ParentClass::PI . PHP_EOL;

$parentClass = new ParentClass();
echo 'Accès à la constante : ' . $parentClass::PI . PHP_EOL;

// --- self --- static --- parent
$parentClass::Parentfunc();

$childClass = new ChildClass();
$childClass::Childfunc();