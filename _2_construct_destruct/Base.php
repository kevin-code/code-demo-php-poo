<?php

namespace _2_construct_destruct;

class Base
{
    // Le constructeur d'une classe est une méthode spéciale qui est appelée à chaque création d'un objet.
    public function __construct(public string $prenom, public string $nom)
    {
    }

    // Le destructeur d'une classe est une méthode spéciale qui est appelée à chaque fois
    // qu'il n'y a plus aucune référence à un objet.
    public function __destruct()
    {
        echo "Aucune référence à l'objet, je suis donc appelé" . PHP_EOL;
    }

    public function direBonjour()
    {
        echo "Bonjour ! Je m'appelle $this->prenom $this->nom" . PHP_EOL;
    }
}

$john = new Base('John', 'Doe');
$kevin = new Base('Kevin', 'Jollis');

echo $john->prenom . PHP_EOL;
$john->direBonjour(); // Bonjour John Doe
$john = $kevin;
unset($kevin); // Bonjour Kevin Jollis
$john->direBonjour();
echo "test" . PHP_EOL;