<?php

namespace _3_heritage;

/**
 * - L'héritage permet d'étendre une classe ou de la rendre plus spécifique.
 * - Elle permet de lier deux classes.
 * - Heritage multiple n'existe pas en PHP
 * - Si la classe enfant ne définit pas de constructeur, celui de sa classe parente sera appelée
 * - Surcharge (propriétés - méthodes)
 * - final permet d'empêcher la surcharge
 */
class ParentHeritage
{
    function __construct(public string $prenom, public string $nom) {}

    function direBonjour() {
        echo "Bonjour, je suis $this->prenom $this->nom" . PHP_EOL;
    }
}

class Heritage extends ParentHeritage
{
    function direBonjour()
    {
        echo "J'ai surchargé la méthode direBonjour()" . PHP_EOL;
    }
}

$heritage = new Heritage('John', 'Doe');
$heritage->direBonjour();