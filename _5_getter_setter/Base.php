<?php

namespace _5_getters_setters;

/**
 * ________________________________________________________________________
 *                          INTRODUCTION
 * ________________________________________________________________________
 *
 * Propriété privée = seule la classe peut la modifier
 *
 * Pour l'initialisation, on peut passer par la méthode __construct, mais on ne peut
 * ni accéder aux propriétés, ni les modifier à l'extérieur de la classe.
 *
 * Donc l'objectif de déclarer des propriétés en private ou protected,
 * c'est d'indiquer à soi-même ou aux autres développeurs la manière dont
 * on modifie les objets d'une classe pour éviter les bugs.
 */
class User
{
    function __construct(private string $prenom, private string $nom)
    {
    }
}

$user1 = new User('John', 'Doe');

/**
 * ________________________________________________________________________
 *                          EXEMPLE DE BUG
 * ________________________________________________________________________
 */
//class ExempleBug
//{
//    public string $nomComplet;
//
//    function __construct(public string $prenom, public string $nom)
//    {
//        $this->nomComplet = $prenom . ' ' . $nom;
//    }
//}
//
//$userBug = new ExempleBug('John', 'Doe');
//echo $userBug->nomComplet . PHP_EOL;
//$userBug->prenom = 'Kevin';
//echo $userBug->nomComplet;

/**
 * ________________________________________________________________________
 *                 GETTERS (ACCESSEURS) - SETTERS (MUTATEURS)
 * ________________________________________________________________________
 *
 * On peut empêcher ce type de bug avec les getters et les setters.
 *
 * Les getters et setters permettent d'accéder ou de modifier des propriétés privées
 * proprement sans endommager le fonctionnement de la classe.
 *
 * Cela revient à créer une interface pour manipuler les classes
 *
 * Ce pattern doit être utilisé à bon escient. Il faut se demander par exemple,
 * est-ce-que la modification de certaines propriétés peut "casser" la classe ?
 * Est-ce-que certaines propriétés ne sont utiles que dans la classe ?
 *
 */
//class UserWithGettersAndSetters
//{
//    private string $nomComplet;
//
//    function __construct(private string $prenom, private string $nom)
//    {
//        $this->nomComplet = $prenom . ' ' . $nom;
//    }
//
//    function setPrenom(string $nouveauPrenom)
//    {
//        $this->prenom = $nouveauPrenom;
//        $this->nomComplet = $nouveauPrenom . ' ' . $this->nom;
//    }
//
//    function setNom(string $nouveauNom)
//    {
//        $this->nom = $nouveauNom;
//        $this->nomComplet = $this->prenom . ' ' . $nouveauNom;
//    }
//
//    function getPrenom()
//    {
//        return $this->prenom;
//    }
//
//    function getNom()
//    {
//        return $this->nom;
//    }
//
//    function getNomComplet()
//    {
//        return $this->nomComplet;
//    }
//}
//
//$userWithGettersAndSetters = new UserWithGettersAndSetters('John', 'Doe');
//echo $userWithGettersAndSetters->getNomComplet() . PHP_EOL; // John Doe
//$userWithGettersAndSetters->setPrenom('Kevin');
//$userWithGettersAndSetters->setNom('Jollis');
//echo $userWithGettersAndSetters->getNomComplet(); // Kevin Jollis

/**
 * ________________________________________________________________________
 *                          Les méthodes magiques __get & __set
 * ________________________________________________________________________
 *
 * Les méthodes magiques sont des méthodes spéciales qui permettent d'exécuter du code lorsque
 * certaines actions sont réalisées sur un objet.
 *
 * __get() est appelée pour lire des données depuis des propriétés protégées, privées ou inexistantes.
 */
class methodesMagiques
{

    private string $nomComplet;

    function __construct(private string $prenom, private string $nom)
    {
        $this->nomComplet = $prenom . ' ' . $nom;
    }

    function __get($prop)
    {
        if (isset($this->$prop)) {
            return $this->$prop;
        } else {
            echo "La propriété n'existe pas !";
        }
    }
}

$methodes = new methodesMagiques('John', 'Doe');
echo $methodes->nomComplet . PHP_EOL; // John doe
echo $methodes->prenom . PHP_EOL; // John
echo $methodes->nom . PHP_EOL; // Doe
echo $methodes->age . PHP_EOL; // La propriété n'existe pas !