<?php

namespace _4_visibilite;

/**
 * ________________________________________________________________________
 *                              VISIBILITE
 * ________________________________________________________________________
 */

/**
 * __________ PUBLIC __________
 */
class User
{
    public string $nom = 'John doe';

    function direBonjour() {
        echo "Bonjour $this->nom !"; // accès depuis la classe
    }
}

$user1 = new User();
$user1->direBonjour(); // accès à l'extérieur de la classe

class Admin extends User {
    function direAurevoir() {
        echo "Au revoir $this->nom !"; // accès depuis la classe enfant
    }
}

/**
 * __________ PRIVATE __________
 */
//class User
//{
//    public string $nom = 'Inconnu';
//
//    private function direBonjour()
//    {
//        echo "Bonjour $this->nom !"; // accès depuis la classe
//    }
//}
//
//$user1 = new User();
//echo $user1->nom; // ERREUR !
//$user1->direBonjour(); // ERREUR !
//
//class Admin extends User
//{
//    function direAurevoir()
//    {
//        echo "Au revoir $this->nom !"; // Indéfini
//    }
//}
//
//$admin = new Admin();
//$admin->direAurevoir(); // Au revoir

/**
 * __________ PROTECTED __________
 */
//class User
//{
//    private string $nom = 'Inconnu';
//
//    public function direBonjour()
//    {
//        echo "Bonjour $this->nom !"; // accès depuis la classe
//    }
//}
//
//$user1 = new User();
//echo $user1->nom; // ERREUR !
//$user1->direBonjour(); // ERREUR !
//
//class Admin extends User
//{
//    function direAurevoir()
//    {
//        echo "Au revoir $this->nom !"; // accès depuis la classe enfant
//    }
//}
//
//$admin = new Admin();
//$admin->direAurevoir(); // Au revoir Inconnu !